Title: Infostand am 11. Oktober (Alte Mensa)
Date: 2022-10-08 16:00
Category: Artikel
Tags: Organisatorisches, FSFW
Slug: Infostand-WiSe22-23
Author: CarK
Summary: Erstes FSFW-Treffen zum Auftakt des Wintersemesters 22/23.
img_url: /img/blog/2022-10-11-infostand.png

Das neue Semester hat begonnen und es gibt potenziell viele neue Studierende und Mitarbeiter:innen mit Interesse an Engagement für Freie Software und Freies Wissen.

Um hierfür eine Anlaufstelle zu bieten, treffen wir uns am **Dienstag, 11.10.2022, von 11:30 bis 13:30** auf der „Get TU-gether Zone“ vor der **Alten Mensa** und präsentieren die FSFW und kommen mit Menschen ins Gespräch.

Karte: <https://osm.org/go/0MLhDLQRF--?layers=N&m=>
